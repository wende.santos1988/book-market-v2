CREATE TABLE book(
    id int auto_increment primary key,
    name varchar(255) not null,
    price decimal(10, 2) not null,
    status varchar(255) not null,
    author_id int not null,
    FOREIGN KEY(author_id) REFERENCES author(id)
);