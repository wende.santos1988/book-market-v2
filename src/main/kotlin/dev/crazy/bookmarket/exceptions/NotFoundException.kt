package dev.crazy.bookmarket.exceptions

import org.springframework.http.HttpStatus

class NotFoundException(message: String?, val httpStatus: HttpStatus): RuntimeException(message)