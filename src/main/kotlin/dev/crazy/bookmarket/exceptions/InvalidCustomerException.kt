package dev.crazy.bookmarket.exceptions

import org.springframework.http.HttpStatus

class InvalidCustomerException(message: String?, val httpStatus: HttpStatus): RuntimeException(message) {

}