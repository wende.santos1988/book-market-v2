package dev.crazy.bookmarket.exceptions

import org.springframework.http.HttpStatus

class BookStatusNotPermittedException(message: String?, val httpStatus: HttpStatus): RuntimeException(message)