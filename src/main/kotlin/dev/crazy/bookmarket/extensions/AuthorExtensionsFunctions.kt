package dev.crazy.bookmarket.extensions

import dev.crazy.bookmarket.api.dtos.request.author.CreateAuthorDTO
import dev.crazy.bookmarket.api.dtos.request.author.UpdateAuthorDTO
import dev.crazy.bookmarket.api.dtos.response.author.AuthorResponseDTO
import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.AuthorStatus

fun CreateAuthorDTO.convertToModel() =
    Author(name = this.name!!, email = this.email!!, status = AuthorStatus.ACTIVE)

fun Author.convertToResponseDTO() =
    AuthorResponseDTO(id = this.id, name = this.name, email = this.email, status = this.status)

fun UpdateAuthorDTO.convertToModel(id: Long) =
    Author(id = id, name = this.name, email = this.email, status = this.status)
