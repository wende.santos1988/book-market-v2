package dev.crazy.bookmarket.extensions

import dev.crazy.bookmarket.api.dtos.request.customer.CreateCustomerDTO
import dev.crazy.bookmarket.api.dtos.response.customer.CustomerResponseDTO
import dev.crazy.bookmarket.domain.entities.Customer
import dev.crazy.bookmarket.domain.enums.CustomerStatus

fun CreateCustomerDTO.convertToModel() =
    Customer(name = this.name!!, email = this.email!!, status = CustomerStatus.ACTIVE)

fun Customer.convertToResponseDTO() =
    CustomerResponseDTO(id = this.id, name = this.name, email = this.email, status = this.status)