package dev.crazy.bookmarket.extensions

import dev.crazy.bookmarket.api.dtos.request.book.CreateBookDTO
import dev.crazy.bookmarket.api.dtos.response.book.BookResponseDTO
import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.BookStatus.ACTIVE

fun CreateBookDTO.convertToModel(author: Author) =
    Book(name = this.name!!, price = this.price!!, author = author, status = ACTIVE)

fun Book.convertToResponseDTO() =
    BookResponseDTO(id = this.id!!, name = this.name, price = this.price, status = this.status!!, author = this.author!!.name)