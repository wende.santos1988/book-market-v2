package dev.crazy.bookmarket.api.controllers

import dev.crazy.bookmarket.api.controllers.mapper.PurchaseMapper
import dev.crazy.bookmarket.api.dtos.request.purchase.PostPurchaseRequest
import dev.crazy.bookmarket.domain.facade.purchase.CreatePurchaseFacade
import dev.crazy.bookmarket.domain.service.PurchaseService
import lombok.RequiredArgsConstructor
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class PurchaseController(
    private val createPurchaseFacade: CreatePurchaseFacade,
    private val purchaseMapper: PurchaseMapper
) {

    object UrlConstants {
        private const val BASE_URL: String = "/purchase"
        const val POST_CREATE_PURCHASE: String = BASE_URL
    }

    @PostMapping(UrlConstants.POST_CREATE_PURCHASE)
    @ResponseStatus(HttpStatus.CREATED)
    fun purchase(@RequestBody @Valid request: PostPurchaseRequest) {
        createPurchaseFacade.createPurchase(purchaseMapper.toModel(request))
    }

}