package dev.crazy.bookmarket.api.controllers.mapper

import dev.crazy.bookmarket.api.dtos.request.purchase.PostPurchaseRequest
import dev.crazy.bookmarket.domain.entities.Purchase
import dev.crazy.bookmarket.domain.facade.customer.FindCustomerFacade
import dev.crazy.bookmarket.domain.service.BookService
import org.springframework.stereotype.Component

@Component
class PurchaseMapper(
    private val FindCustomerFacade: FindCustomerFacade,
    private val bookService: BookService
) {

    fun toModel(request: PostPurchaseRequest): Purchase {
        val customer = FindCustomerFacade.findCustomerById(request.customerId)
        val books = bookService.findBookByIds(request.bookIds)

        return Purchase(
            customer = customer,
            books = books,
            price = books.sumOf { it.price }
        )
    }

}