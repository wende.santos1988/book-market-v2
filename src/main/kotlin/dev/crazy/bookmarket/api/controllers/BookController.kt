package dev.crazy.bookmarket.api.controllers

import dev.crazy.bookmarket.api.dtos.request.book.CreateBookDTO
import dev.crazy.bookmarket.api.dtos.request.book.UpdateBookDOT
import dev.crazy.bookmarket.api.dtos.response.book.BookResponseDTO
import dev.crazy.bookmarket.domain.facade.book.CreateBookFacade
import dev.crazy.bookmarket.domain.facade.book.FindBookFacade
import dev.crazy.bookmarket.domain.facade.book.UpdateBookFacade
import dev.crazy.bookmarket.extensions.convertToResponseDTO
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class BookController(
    private val createBookFacade: CreateBookFacade,
    private val findBookFacade: FindBookFacade,
    private val updateBookFacade: UpdateBookFacade
) {

    object UrlConstants {
        const val BASE_URL: String = "/book"
        const val POST_CREATE_BOOK: String = BASE_URL
        const val FIND_ACTIVES_BOOK: String = "$BASE_URL/actives"
        const val FIND_BOOK_BY_ID: String = "$BASE_URL/{id}"
        const val PUT_UPDATE_BOOK_BY_ID: String = "$BASE_URL/{id}"
        const val DELETE_BOOK_BY_ID: String = "$BASE_URL/{id}"
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(UrlConstants.POST_CREATE_BOOK)
    fun create(@RequestBody @Valid request: CreateBookDTO): BookResponseDTO =
        createBookFacade.createBook(request).convertToResponseDTO()

    @GetMapping(UrlConstants.BASE_URL)
    fun findAllBooks(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponseDTO> =
        findBookFacade.findAllBook(pageable).map { it.convertToResponseDTO() }

    @GetMapping(UrlConstants.FIND_ACTIVES_BOOK)
    fun findAllBooksActives(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponseDTO> =
        findBookFacade.findAllBooksActives(pageable).map { it.convertToResponseDTO() }

    @GetMapping(UrlConstants.FIND_BOOK_BY_ID)
    fun findBookById(@PathVariable id: Long): BookResponseDTO =
        findBookFacade.findBookById(id).convertToResponseDTO()

    @PutMapping(UrlConstants.PUT_UPDATE_BOOK_BY_ID)
    fun updateBookById(@PathVariable id: Long, @RequestBody updateBookDOT: UpdateBookDOT): BookResponseDTO =
        updateBookFacade.updateBookFacade(updateBookDOT).convertToResponseDTO()


}