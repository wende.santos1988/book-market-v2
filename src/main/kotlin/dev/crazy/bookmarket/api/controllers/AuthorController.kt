package dev.crazy.bookmarket.api.controllers

import dev.crazy.bookmarket.api.dtos.request.author.CreateAuthorDTO
import dev.crazy.bookmarket.api.dtos.request.author.UpdateAuthorDTO
import dev.crazy.bookmarket.api.dtos.response.author.AuthorResponseDTO
import dev.crazy.bookmarket.domain.facade.author.CreateAuthorFacade
import dev.crazy.bookmarket.domain.facade.author.DeleteAuthorFacade
import dev.crazy.bookmarket.domain.facade.author.FindAuthorFacade
import dev.crazy.bookmarket.domain.facade.author.UpdateAuthorFacade
import dev.crazy.bookmarket.extensions.convertToModel
import dev.crazy.bookmarket.extensions.convertToResponseDTO
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class AuthorController(
    private val createAuthorFacade: CreateAuthorFacade,
    private val findAuthorFacade: FindAuthorFacade,
    private val updateAuthorFacade: UpdateAuthorFacade,
    private val deleteAuthorFacade: DeleteAuthorFacade
) {
    object UrlConstants {
        private const val BASE_URL: String = "/author"
        const val POST_CREATE_AUTHOR: String = BASE_URL
        const val GET_AUTHOR_BY_ID: String = "$BASE_URL/{id}"
        const val GET_ALL_AUTHOR: String = "$BASE_URL"
        const val UPDATE_AUTHOR_BY_ID: String = "$BASE_URL/{id}"
        const val DELETE_AUTHOR_BY_ID: String = "$BASE_URL/{id}"
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(UrlConstants.POST_CREATE_AUTHOR)
    fun createAuthor(@RequestBody @Valid createAuthorDTO: CreateAuthorDTO): AuthorResponseDTO =
        createAuthorFacade.createAuthor(createAuthorDTO.convertToModel()).convertToResponseDTO()


    @GetMapping(UrlConstants.GET_AUTHOR_BY_ID)
    fun findAuthorById(@PathVariable id: Long): AuthorResponseDTO =
        findAuthorFacade.findAuthorById(id).convertToResponseDTO()

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(UrlConstants.UPDATE_AUTHOR_BY_ID)
    fun update(@PathVariable id: Long, @RequestBody updateAuthorDTO: UpdateAuthorDTO): AuthorResponseDTO =
        updateAuthorFacade.updateAuthorFacade(updateAuthorDTO.convertToModel(id)).convertToResponseDTO()

    @GetMapping(UrlConstants.GET_ALL_AUTHOR)
    fun findAllActivesAuthors(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<AuthorResponseDTO> =
        findAuthorFacade.findAllActivesAuthors(pageable).map { it.convertToResponseDTO() }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(UrlConstants.DELETE_AUTHOR_BY_ID)
    fun deleteAuthor(@PathVariable id: Long) = deleteAuthorFacade.deleteAuthor(id)
}