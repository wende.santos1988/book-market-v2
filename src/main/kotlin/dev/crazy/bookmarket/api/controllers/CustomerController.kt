package dev.crazy.bookmarket.api.controllers

import dev.crazy.bookmarket.api.dtos.request.customer.CreateCustomerDTO
import dev.crazy.bookmarket.api.dtos.request.customer.UpdateCustomerDOT
import dev.crazy.bookmarket.api.dtos.response.customer.CustomerResponseDTO
import dev.crazy.bookmarket.domain.facade.customer.CreateCustomerFacade
import dev.crazy.bookmarket.domain.facade.customer.DeleteCustomerFacade
import dev.crazy.bookmarket.domain.facade.customer.FindCustomerFacade
import dev.crazy.bookmarket.domain.facade.customer.UpdateCustomerFacade
import dev.crazy.bookmarket.extensions.convertToModel
import dev.crazy.bookmarket.extensions.convertToResponseDTO
import lombok.RequiredArgsConstructor
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class CustomerController(
    private val createCustomerFacade: CreateCustomerFacade,
    private val findCustomerFacade: FindCustomerFacade,
    private val updateCustomerFacade: UpdateCustomerFacade,
    private val deleteCustomerFacade: DeleteCustomerFacade
) {

    object UrlConstants {
        const val BASE_URL: String = "/customer"
        const val POST_CREATE_CUSTOMER: String = BASE_URL
        const val GET_FIND_BY_ID: String = "$BASE_URL/{id}"
        const val PUT_UPDATE_BY_ID: String = "$BASE_URL/{id}"
        const val DELETE_BY_ID: String = "$BASE_URL/{id}"
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(UrlConstants.POST_CREATE_CUSTOMER)
    fun create(@RequestBody @Valid createCustomerDTO: CreateCustomerDTO): CustomerResponseDTO =
        createCustomerFacade.createBook(createCustomerDTO.convertToModel()).convertToResponseDTO()

    @GetMapping(UrlConstants.GET_FIND_BY_ID)
    fun findCustomerById(@PathVariable id: Long): CustomerResponseDTO =
        findCustomerFacade.findCustomerById(id).convertToResponseDTO()

    @GetMapping(UrlConstants.BASE_URL)
    fun findAllCustomersActives(@RequestParam name: String?): List<CustomerResponseDTO>? =
        findCustomerFacade.findAllCustomer(name).map { it.convertToResponseDTO() }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(UrlConstants.PUT_UPDATE_BY_ID)
    fun update(@PathVariable id: Long, @RequestBody customerDtoUpdate: UpdateCustomerDOT): CustomerResponseDTO =
        updateCustomerFacade.updateCustomerFacade(customerDtoUpdate).convertToResponseDTO()

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(UrlConstants.DELETE_BY_ID)
    fun delete(@PathVariable id: Long) = deleteCustomerFacade.delete(id)
}