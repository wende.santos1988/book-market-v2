package dev.crazy.bookmarket.api.dtos.request.author

import dev.crazy.bookmarket.domain.enums.AuthorStatus

class UpdateAuthorDTO (var id: Long, var name: String, var email: String, val status:AuthorStatus)
