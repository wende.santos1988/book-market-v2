package dev.crazy.bookmarket.api.dtos.response.customer

import dev.crazy.bookmarket.domain.enums.CustomerStatus

class CustomerResponseDTO(
    val id: Long? = null,
    val name: String,
    val email: String,
    val status: CustomerStatus
)
