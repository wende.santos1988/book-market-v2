package dev.crazy.bookmarket.api.dtos.request.book

import com.fasterxml.jackson.annotation.JsonAlias
import java.math.BigDecimal
import javax.validation.constraints.NotNull

class CreateBookDTO (
    @field: NotNull var name: String?,
    @field: NotNull var price: BigDecimal?,

    @JsonAlias("author_id")
    @field: NotNull val authorId: Long
)