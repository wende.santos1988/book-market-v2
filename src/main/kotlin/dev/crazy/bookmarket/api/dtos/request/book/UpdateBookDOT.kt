package dev.crazy.bookmarket.api.dtos.request.book

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.BookStatus
import java.math.BigDecimal

class UpdateBookDOT (
    var id: Long,
    var name: String,
    var price: BigDecimal,
    var status: BookStatus,
    var author: Author
)