package dev.crazy.bookmarket.api.dtos.request.customer

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class CreateCustomerDTO (
    @field: NotEmpty(message = "must not be empty")
    val name: String?,

    @field: Email(message = "must be valid")
    @field: NotNull(message = "must not be empty")
    var email: String?
)