package dev.crazy.bookmarket.api.dtos.request.purchase

import com.fasterxml.jackson.annotation.JsonAlias
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class PostPurchaseRequest(
    @field: NotNull
    @field: Positive
    @JsonAlias()
    val customerId: Long,

    @field: NotNull
    val bookIds: Set<Long>



)