package dev.crazy.bookmarket.api.dtos.response

data class ErrorResponse(
    var httpCode: Int,
    var message: String,
)
