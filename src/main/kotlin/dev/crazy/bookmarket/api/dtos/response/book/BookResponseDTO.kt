package dev.crazy.bookmarket.api.dtos.response.book

import dev.crazy.bookmarket.domain.enums.BookStatus
import java.math.BigDecimal

class BookResponseDTO(
    var id: Long,
    var name: String,
    var price: BigDecimal,
    var status: BookStatus,
    var author: String

)