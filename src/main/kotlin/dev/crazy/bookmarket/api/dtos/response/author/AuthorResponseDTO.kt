package dev.crazy.bookmarket.api.dtos.response.author

import dev.crazy.bookmarket.domain.enums.AuthorStatus

data class AuthorResponseDTO(
    val id: Long? = null,
    val name: String,
    val email: String,
    val status: AuthorStatus
)
