package dev.crazy.bookmarket.events

import dev.crazy.bookmarket.domain.entities.Purchase
import org.springframework.context.ApplicationEvent

class PurchaseEvent(
    source: Any,
    val purchase: Purchase
): ApplicationEvent(source) {

}