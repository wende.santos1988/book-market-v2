package dev.crazy.bookmarket.events

import dev.crazy.bookmarket.domain.facade.purchase.UpdatePurchaseFacade
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.util.*

@Component
class GenerateNfeListener(
    private val updatePurchaseFacade: UpdatePurchaseFacade
) {

    @EventListener
    fun listen(purchaseEvent: PurchaseEvent) {
        val nfe = UUID.randomUUID().toString()
        val purchase = purchaseEvent.purchase.copy(nfe = nfe)
        updatePurchaseFacade.updateNfePurchase(purchase)
    }
}