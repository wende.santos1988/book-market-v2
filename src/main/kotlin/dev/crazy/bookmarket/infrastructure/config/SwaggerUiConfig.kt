package dev.crazy.bookmarket.infrastructure.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerUiConfig {

    @Bean
    fun gettingApi(): Docket = Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build()
        .apiInfo(metaData())
        .useDefaultResponseMessages(false)


    private fun metaData(): ApiInfo = ApiInfoBuilder()
        .title("BOOK MARKET API")
        .description("API to management of book market.")
        .version("0.0.1")
        .contact(
            Contact(
                "Wendel Santos",
                "https://gitlab.com/wende.santos1988",
                "henrique.santos1988@gmail.com"))
        .build()
}