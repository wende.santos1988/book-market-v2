package dev.crazy.bookmarket.advicers

import dev.crazy.bookmarket.exceptions.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class BusinessControllerAdvice {

    @ExceptionHandler(BusinessException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleBusinessException(exception: BusinessException): ApiErrors {
        return ApiErrors(exception)
    }

    @ExceptionHandler(InvalidCustomerException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleInvalidCustomerException(exception: InvalidCustomerException): ApiErrors {
        return ApiErrors(exception)
    }

    @ExceptionHandler(BookStatusNotPermittedException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleBookStatusInvalidException(exception: BookStatusNotPermittedException): ApiErrors {
        return ApiErrors(exception)
    }

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFoundException(exception: NotFoundException): ApiErrors {
        return ApiErrors(exception)
    }

}