package dev.crazy.bookmarket.domain.service

import dev.crazy.bookmarket.domain.entities.Purchase
import dev.crazy.bookmarket.domain.repositories.PurchaseRepository
import dev.crazy.bookmarket.events.PurchaseEvent
import lombok.RequiredArgsConstructor
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class PurchaseService(
    private val purchaseRepository: PurchaseRepository,
    private val applicationEventPublisher: ApplicationEventPublisher
) {

    fun savePurchase(purchase: Purchase) {
        purchaseRepository.save(purchase)
    }

    fun create(purchase: Purchase) {
        savePurchase(purchase)
        applicationEventPublisher.publishEvent(PurchaseEvent(this, purchase))
    }

    fun update(purchase: Purchase) {
        savePurchase(purchase)
    }
}