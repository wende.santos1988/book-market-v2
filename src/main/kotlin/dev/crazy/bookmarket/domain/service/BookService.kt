package dev.crazy.bookmarket.domain.service

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.BookStatus
import dev.crazy.bookmarket.domain.repositories.BookRepository
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.Optional

@Service
@RequiredArgsConstructor
class BookService(
    val bookRepository: BookRepository
) {

    fun saveBook(book: Book): Book {
        return bookRepository.save(book)
    }

    fun findAllBooks(pageable: Pageable): Page<Book> {
        return bookRepository.findAll(pageable)
    }

    fun findAllBooksActives(pageable: Pageable): Page<Book> {
        return bookRepository.findByStatus(BookStatus.ACTIVE, pageable)
    }

    fun findBookById(id: Long): Optional<Book> = bookRepository.findById(id)

    fun findBookByIds(bookIds: Set<Long>): List<Book> {
        return bookRepository.findAllById(bookIds).toList()
    }

    fun deleteByAuthor(author: Author) {
        val books = bookRepository.findByAuthor(author)
        for (book in books) {
            book.status = BookStatus.DELETED
        }
        bookRepository.saveAll(books)
    }
}
