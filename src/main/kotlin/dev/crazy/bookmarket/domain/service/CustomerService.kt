package dev.crazy.bookmarket.domain.service

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.entities.Customer
import dev.crazy.bookmarket.domain.enums.AuthorStatus
import dev.crazy.bookmarket.domain.enums.CustomerStatus
import dev.crazy.bookmarket.domain.repositories.CustomerRepository
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service
import java.util.Optional

@Service
@RequiredArgsConstructor
class CustomerService(
    val customerRepository: CustomerRepository,
) {

    fun findCustomerById(id: Long): Optional<Customer> {
        return customerRepository.findById(id)
    }

    fun findCustomersName(name: String): List<Customer> = customerRepository.findByName(name)

    fun findCustomersByName(): List<Customer> = customerRepository.findAll().toList()

    fun saveCustomer(customerToSave: Customer): Customer = customerRepository.save(customerToSave)

    fun deleteLogicCustomer(customerToDelete: Customer) {
        customerToDelete.status = CustomerStatus.INACTIVE
        saveCustomer(customerToDelete)
    }

    fun emailIsAvailable(value: String): Boolean {
        return customerRepository.existsByEmail(value)
    }

}