package dev.crazy.bookmarket.domain.service

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.AuthorStatus
import dev.crazy.bookmarket.domain.repositories.AuthorRepository
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.Optional

@Service
@RequiredArgsConstructor
class AuthorService(
    val authorRepository: AuthorRepository
) {

    fun saveAuthor(author: Author) = authorRepository.save(author)

    fun findAuthorById(id: Long): Optional<Author> = authorRepository.findById(id)

    fun findAllCustomer(name: String?): List<Author> {
        name?.let {
            return authorRepository.findByName(name)
        }
        return authorRepository.findAll().toList()
    }

    fun findAllAuthorActives(pageable: Pageable): Page<Author> {
        return authorRepository.findByStatus(AuthorStatus.ACTIVE, pageable)
    }

    fun deleteLogicAuthor(authorToDelete: Author) {
        authorToDelete.status = AuthorStatus.INACTIVE
        saveAuthor(authorToDelete)
    }

    fun authorExistsByEmail(authorEmail: String) = authorRepository.existsByEmail(authorEmail)

    fun emailIsAvailable(value: String): Boolean = !authorRepository.existsByEmail(value)

}