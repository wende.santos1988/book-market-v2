package dev.crazy.bookmarket.domain.enums

enum class AuthorStatus {
    ACTIVE,
    INACTIVE
}
