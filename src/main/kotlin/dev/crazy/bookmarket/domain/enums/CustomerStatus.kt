package dev.crazy.bookmarket.domain.enums

enum class CustomerStatus {
    ACTIVE,
    INACTIVE
}
