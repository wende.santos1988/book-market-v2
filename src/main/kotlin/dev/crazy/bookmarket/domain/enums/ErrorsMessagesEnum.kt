package dev.crazy.bookmarket.domain.enums

import org.springframework.http.HttpStatus

enum class ErrorsMessagesEnum(val message: String, val httpStatus: HttpStatus) {
    BOOK_NOT_FOUND("Book with id [%s] not found.", HttpStatus.NOT_FOUND),
    AUTHOR_NOT_FOUND("Author with id [%s] not found.", HttpStatus.NOT_FOUND),
    CUSTOMER_NOT_FOUND("Customer with id [%s] not found.", HttpStatus.NOT_FOUND),
    BOOK_STATUS_NOT_PERMITTED("book with status [%s] not permitted update", HttpStatus.BAD_REQUEST),
    EMAIL_ALREADY_EXISTS("E-mail [%s] already registered", HttpStatus.BAD_REQUEST)
}