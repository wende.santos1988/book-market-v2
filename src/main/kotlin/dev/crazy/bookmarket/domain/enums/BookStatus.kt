package dev.crazy.bookmarket.domain.enums

enum class BookStatus {
    ACTIVE,
    SOLD,
    CANCELED,
    DELETED
}