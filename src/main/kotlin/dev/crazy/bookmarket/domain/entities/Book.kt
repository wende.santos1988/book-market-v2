package dev.crazy.bookmarket.domain.entities

import dev.crazy.bookmarket.domain.enums.BookStatus
import java.math.BigDecimal
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
data class Book(

    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    @Column
    var name: String,

    @Column
    var price: BigDecimal,

    @ManyToOne
    @JoinColumn(name = "author_id")
    var author: Author? = null
) {

    @Column
    @Enumerated(EnumType.STRING)
    var status: BookStatus? = null

    constructor(
        id: Long? = null,
        name: String,
        price: BigDecimal,
        status: BookStatus?,
        author: Author? = null,
    ): this(id, name, price, author) {
        this.status = status
    }
}
