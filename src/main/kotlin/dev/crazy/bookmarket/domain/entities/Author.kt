package dev.crazy.bookmarket.domain.entities

import dev.crazy.bookmarket.domain.enums.AuthorStatus
import javax.persistence.*
import javax.persistence.EnumType.STRING
import javax.persistence.GenerationType.IDENTITY

@Entity
data class Author(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    @Column
    var name: String,

    @Column
    var email: String,

    @Column
    @Enumerated(STRING)
    var status: AuthorStatus
)
