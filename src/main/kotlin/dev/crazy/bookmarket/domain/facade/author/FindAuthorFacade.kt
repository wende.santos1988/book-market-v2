package dev.crazy.bookmarket.domain.facade.author

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.AuthorStatus.INACTIVE
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.AUTHOR_NOT_FOUND
import dev.crazy.bookmarket.domain.service.AuthorService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class FindAuthorFacade(
    private val authorService: AuthorService
) {
    fun findAuthorById(id: Long): Author {
        val authorSaved = authorService.findAuthorById(id)
        if(!authorSaved.isEmpty && authorSaved.get().status == INACTIVE) {
            throw NotFoundException(
                AUTHOR_NOT_FOUND.message.format(id),
                AUTHOR_NOT_FOUND.httpStatus
            )
        }
        return authorSaved.get()
    }

    fun findAllActivesAuthors(pageable: Pageable): Page<Author> {
        return authorService.findAllAuthorActives(pageable)
    }
}