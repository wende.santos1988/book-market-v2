package dev.crazy.bookmarket.domain.facade.customer

import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.CUSTOMER_NOT_FOUND
import dev.crazy.bookmarket.domain.service.CustomerService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class DeleteCustomerFacade(
    private val customerService: CustomerService
) {
    fun delete(id: Long) {
        val customerToDelete = customerService.findCustomerById(id).orElseThrow {
            NotFoundException(
                CUSTOMER_NOT_FOUND.message.format(id),
                CUSTOMER_NOT_FOUND.httpStatus
            )
        }
        customerService.deleteLogicCustomer(customerToDelete)
    }
}
