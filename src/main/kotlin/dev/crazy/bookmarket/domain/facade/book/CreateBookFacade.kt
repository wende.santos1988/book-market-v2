package dev.crazy.bookmarket.domain.facade.book

import dev.crazy.bookmarket.api.dtos.request.book.CreateBookDTO
import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.CUSTOMER_NOT_FOUND
import dev.crazy.bookmarket.domain.service.AuthorService
import dev.crazy.bookmarket.domain.service.BookService
import dev.crazy.bookmarket.exceptions.NotFoundException
import dev.crazy.bookmarket.extensions.convertToModel
import dev.crazy.bookmarket.extensions.convertToResponseDTO
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class CreateBookFacade(
    private val authorService: AuthorService,
    private val bookService: BookService
) {
    fun createBook(createBookDTO: CreateBookDTO): Book {
        val authorSaved = authorService.findAuthorById(createBookDTO.authorId)
            .orElseThrow {
                NotFoundException(
                    CUSTOMER_NOT_FOUND.message.format(createBookDTO.authorId),
                    CUSTOMER_NOT_FOUND.httpStatus
                )
            }
        return bookService.saveBook(createBookDTO.convertToModel(authorSaved))
    }
}