package dev.crazy.bookmarket.domain.facade.customer

import dev.crazy.bookmarket.domain.entities.Customer
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum
import dev.crazy.bookmarket.domain.service.CustomerService
import dev.crazy.bookmarket.exceptions.BusinessException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class CreateCustomerFacade(
    private val customerService: CustomerService
) {

    fun createBook(customer: Customer): Customer {
        if (customerService.emailIsAvailable(customer.email)) {
            throw BusinessException(
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.message.format(customer.email),
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.httpStatus
            )
        }
        return customerService.saveCustomer(customer)
    }

}