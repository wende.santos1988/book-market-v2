package dev.crazy.bookmarket.domain.facade.purchase

import dev.crazy.bookmarket.domain.entities.Purchase
import dev.crazy.bookmarket.domain.service.PurchaseService
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class CreatePurchaseFacade(
    private val purchaseService: PurchaseService
) {
    fun createPurchase(purchase: Purchase) {
        purchaseService.create(purchase)
    }
}