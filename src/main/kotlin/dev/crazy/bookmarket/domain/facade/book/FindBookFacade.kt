package dev.crazy.bookmarket.domain.facade.book

import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.BookStatus
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.BOOK_NOT_FOUND
import dev.crazy.bookmarket.domain.service.BookService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class FindBookFacade(
    private val bookService: BookService
) {
    fun findAllBook(pageable: Pageable): Page<Book> {
        return bookService.findAllBooks(pageable)
    }

    fun findAllBooksActives(pageable: Pageable): Page<Book> {
        return bookService.findAllBooksActives(pageable)
    }

    fun findBookById(id: Long): Book {
        val bookSaved = bookService.findBookById(id)
        if(!bookSaved.isEmpty && bookSaved.get().status == BookStatus.DELETED) {
            throw NotFoundException(
                BOOK_NOT_FOUND.message.format(id),
                BOOK_NOT_FOUND.httpStatus
            )
        }
        return bookSaved.get()
    }
}
