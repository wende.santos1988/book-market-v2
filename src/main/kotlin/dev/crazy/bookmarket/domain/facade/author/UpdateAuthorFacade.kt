package dev.crazy.bookmarket.domain.facade.author

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.AUTHOR_NOT_FOUND
import dev.crazy.bookmarket.domain.service.AuthorService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class UpdateAuthorFacade(
    private val authorService: AuthorService
) {
    fun updateAuthorFacade(authorToUpdate: Author): Author {
        val authorSavedToUpdate = authorService.findAuthorById(authorToUpdate.id!!).orElseThrow {
            NotFoundException(
                AUTHOR_NOT_FOUND.message.format(authorToUpdate.id!!),
                AUTHOR_NOT_FOUND.httpStatus
            )
        }
        authorSavedToUpdate.name = authorToUpdate.name
        authorSavedToUpdate.email = authorToUpdate.email
        return authorService.saveAuthor(authorSavedToUpdate)
    }
}
