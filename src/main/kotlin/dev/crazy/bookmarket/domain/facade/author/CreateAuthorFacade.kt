package dev.crazy.bookmarket.domain.facade.author

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum
import dev.crazy.bookmarket.domain.service.AuthorService
import dev.crazy.bookmarket.exceptions.BusinessException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class CreateAuthorFacade(
    private val authorService: AuthorService
) {

    fun createAuthor(author: Author): Author {
        if (authorService.authorExistsByEmail(author.email)) {
            throw BusinessException(
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.message.format(author.email),
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.httpStatus
            )
        }

        return authorService.saveAuthor(author)
    }

}