package dev.crazy.bookmarket.domain.facade.customer

import dev.crazy.bookmarket.api.dtos.response.customer.CustomerResponseDTO
import dev.crazy.bookmarket.domain.entities.Customer
import dev.crazy.bookmarket.domain.enums.CustomerStatus.INACTIVE
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.CUSTOMER_NOT_FOUND
import dev.crazy.bookmarket.domain.service.CustomerService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class FindCustomerFacade(
    private val customerService: CustomerService
) {

    fun findCustomerById(id: Long): Customer {
        val customerSaved = customerService.findCustomerById(id)
        if(!customerSaved.isEmpty && customerSaved.get().status == INACTIVE) {
            throw NotFoundException(
                CUSTOMER_NOT_FOUND.message.format(id),
                CUSTOMER_NOT_FOUND.httpStatus
            )
        }
        return customerSaved.get()
    }

    fun findAllCustomer(name: String?): List<Customer> {
        name?.let {
            return customerService.findCustomersName(name)
        }
        return customerService.findCustomersByName().toList()
    }

}
