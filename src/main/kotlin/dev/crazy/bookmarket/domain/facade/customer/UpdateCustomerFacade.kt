package dev.crazy.bookmarket.domain.facade.customer

import dev.crazy.bookmarket.api.dtos.request.customer.UpdateCustomerDOT
import dev.crazy.bookmarket.domain.entities.Customer
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.CUSTOMER_NOT_FOUND
import dev.crazy.bookmarket.domain.service.CustomerService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class UpdateCustomerFacade(
    private val customerService: CustomerService
) {

    fun updateCustomerFacade(updateCustomerDOT: UpdateCustomerDOT): Customer {
        val customerSavedToUpdate = customerService.findCustomerById(updateCustomerDOT.id).orElseThrow {
            NotFoundException(
                CUSTOMER_NOT_FOUND.message.format(updateCustomerDOT.id),
                CUSTOMER_NOT_FOUND.httpStatus
            )
        }
        customerSavedToUpdate.name = updateCustomerDOT.name
        customerSavedToUpdate.email = updateCustomerDOT.email
        return customerService.saveCustomer(customerSavedToUpdate)
    }

}
