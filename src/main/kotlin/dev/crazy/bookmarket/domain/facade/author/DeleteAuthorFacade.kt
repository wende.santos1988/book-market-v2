package dev.crazy.bookmarket.domain.facade.author

import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.AUTHOR_NOT_FOUND
import dev.crazy.bookmarket.domain.service.AuthorService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class DeleteAuthorFacade(
    private val authorService: AuthorService
) {
    fun deleteAuthor(id: Long) {
        val authorToDelete = authorService.findAuthorById(id).orElseThrow {
            NotFoundException(
                AUTHOR_NOT_FOUND.message.format(id),
                AUTHOR_NOT_FOUND.httpStatus
            )
        }
        authorService.deleteLogicAuthor(authorToDelete)
    }
}
