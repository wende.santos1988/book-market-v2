package dev.crazy.bookmarket.domain.facade.book

import dev.crazy.bookmarket.api.dtos.request.book.UpdateBookDOT
import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.ErrorsMessagesEnum.BOOK_NOT_FOUND
import dev.crazy.bookmarket.domain.service.BookService
import dev.crazy.bookmarket.exceptions.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service

@Service
@RequiredArgsConstructor
class UpdateBookFacade(
    private val bookService: BookService
) {
    fun updateBookFacade(bookToUpdate: UpdateBookDOT): Book {
        val bookSavedToUpdate = bookService.findBookById(bookToUpdate.id)
            .orElseThrow {
                NotFoundException(
                    BOOK_NOT_FOUND.message.format(bookToUpdate.id),
                    BOOK_NOT_FOUND.httpStatus
                )
            }

        bookSavedToUpdate.author = bookToUpdate.author
        bookSavedToUpdate.name = bookToUpdate.name
        bookSavedToUpdate.price = bookToUpdate.price
        return bookService.saveBook(bookSavedToUpdate)
    }
}