package dev.crazy.bookmarket.domain.repositories

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.enums.AuthorStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AuthorRepository: JpaRepository<Author, Long> {

    @Query("SELECT c FROM Customer c WHERE name LIKE '%'||?1||'%'")
    fun findByName(name: String): List<Author>

    fun existsByEmail(email: String): Boolean

    fun findByStatus(actives: AuthorStatus, pageable: Pageable): Page<Author>
}
