package dev.crazy.bookmarket.domain.repositories

import dev.crazy.bookmarket.domain.entities.Author
import dev.crazy.bookmarket.domain.entities.Book
import dev.crazy.bookmarket.domain.enums.BookStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository: JpaRepository<Book, Long> {
    fun findByStatus(active: BookStatus, pageable: Pageable): Page<Book>
    fun findByAuthor(author: Author): List<Book>
}