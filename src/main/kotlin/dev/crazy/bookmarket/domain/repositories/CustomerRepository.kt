package dev.crazy.bookmarket.domain.repositories

import dev.crazy.bookmarket.domain.entities.Customer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CustomerRepository: JpaRepository<Customer, Long> {

    @Query("SELECT c FROM Customer c WHERE name LIKE '%'||?1||'%'")
    fun findByName(name: String): List<Customer>

    fun existsByEmail(email: String): Boolean

}