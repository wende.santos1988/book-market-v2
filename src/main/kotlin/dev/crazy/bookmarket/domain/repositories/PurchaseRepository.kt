package dev.crazy.bookmarket.domain.repositories

import dev.crazy.bookmarket.domain.entities.Purchase
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PurchaseRepository: JpaRepository<Purchase, Long> {

}
